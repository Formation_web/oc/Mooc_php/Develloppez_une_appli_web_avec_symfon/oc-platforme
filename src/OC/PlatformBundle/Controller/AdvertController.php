<?php

namespace OC\PlatformBundle\Controller;

use OC\PlatformBundle\Entity\Advert;
use OC\PlatformBundle\Entity\AdvertSkill;
use OC\PlatformBundle\Entity\Application;
use OC\PlatformBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Validator\Constraints\DateTime;


class AdvertController extends Controller
{
    /**
     * Index Action
     *
     * @return Response
     */
    public function indexAction($page)
    {
        if ($page < 1) {
            throw new NotFoundHttpException('Page "' . $page . '" inexistante !');
        }

        // Notre liste d'annonce en dur
        $listAdverts = array(
            array(
                'title'   => 'Recherche développpeur Symfony',
                'id'      => 1,
                'author'  => 'Alexandre',
                'content' => 'Nous recherchons un développeur Symfony débutant sur Lyon. Blabla…',
                'date'    => new \Datetime()),
            array(
                'title'   => 'Mission de webmaster',
                'id'      => 2,
                'author'  => 'Hugo',
                'content' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
                'date'    => new \Datetime()),
            array(
                'title'   => 'Offre de stage webdesigner',
                'id'      => 3,
                'author'  => 'Mathieu',
                'content' => 'Nous proposons un poste pour webdesigner. Blabla…',
                'date'    => new \Datetime())
        );

        // On appelle le template
        return $this->render('OCPlatformBundle:Advert:index.html.twig',
            array(
                'listAdverts' => $listAdverts
            )
        );

    }

    /**
     * View Action
     *
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function viewAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        // On récupère l'entité correspondant à $id
        $advert = $em->getRepository('OCPlatformBundle:Advert')->find($id);

        // $advert est donc une instance de OC\PlatformBundle\ENtity\Advert
        // ou Null si l'id $id n'existe pas, d'ou ce if
        if( $advert === null) {
            throw new NotFoundHttpException("L'annonce d'id " . $id . " n'existe pas.");
        }

        //On récupère la liste des candidature de cette annonce
        $listApplications = $em
            ->getRepository('OCPlatformBundle:Application')
            ->findBy(
                array('advert' => $advert)
            )
        ;


            // On récupère maintenant la liste des AdvertSkill
        $listAdvertSkills = $em
            ->getRepository('OCPlatformBundle:AdvertSkill')
            ->findBy(array('advert' => $advert))
        ;

        return $this->render('OCPlatformBundle:Advert:view.html.twig',
            array(
                'advert'            => $advert,
                'listApplications'  => $listApplications ,
                'listAdvertSkills' => $listAdvertSkills
            )
        );
    }

    /**
     * Add Action
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function  addAction(Request $request)
    {
        // On récupère l'entityManager
        $em = $this->getDoctrine()->getManager();

        // Création de l'entité
        $advert = new Advert();
        $advert->setTitle('Recherche développeur symfony');
        $advert->setAuthor('Alexandre');
        $advert->setContent("Nous recherchon un developpeur symfony débutant sur Lyon.");
        // On peut ne pas définir ni la date ni la publication
        // car ces attributs sont définit automatiquement dans le constructeur

        // On récupère toute les compétences
        $listSkills = $em->getRepository('OCPlatformBundle:Skill')->findAll();

        // Pour chaque compétences
        foreach ($listSkills as $skill) {
            // On crée une nouvelle relation entre1 annonce et 1 compétence
            $advertSkill = new AdvertSkill();

            // On la lie a l'annonce qui est toujours la meme
            $advertSkill->setAdvert($advert);

            // On la lie à la compétence, qui change ici dans la boucke foreach
            $advertSkill->setSkill($skill);

            // Arbitrairement, on dit que chaque compétence est requise au niveau expert
            $advertSkill->setLevel('Expert') ;

            // Et on persiste cette entité de relation, propriétaire des deux autre relation
            $em->persist($advertSkill);
        }

        // Doctrine ne connait pas encore l'entité $advert. Si vous n'avez pas défini la relation AdvertSkill
        // avec un cascade persist (ce qui est le cas si vous avez utilisé mon code), alors on doit persister $advert
        $em->persist($advert);

        // On déclenche l'enregistrement
        $em->flush();// Doctrine ne connait pas encore l'entite $advert. Si vous n'avez pas definni la relation

        // Création de l'entité Image
        $image = new Image();
        $image->setUrl('http://sdz-upload.s3.amazonaws.com/prod/upload/job-de-reve.jpg');
        $image->setAlt('Job de rêve');

        // On lie l'image à l'annonce
        $advert->setImage($image);

        // Création d'une première candidature
        $application1 = new Application();
        $application1->setAuthor('Marine');
        $application1->setContent("J'ai toutes les qualités requises !");

        // Création d'une seconde candidature
        $application2 = new Application();
        $application2->setAuthor('Pierre');
        $application2->setContent("Je suis très motivé !");
        
        // On lie les candidature à l'annonce
        $application1->setAdvert($advert);
        $application2->setAdvert($advert);


        // Etape 1 : Onn persiste l'entité
        $em->persist($advert);

        // Etape 1 bis : Si on n'avais pas défini le cascade={"persist"},
        // On devrait persister à la main l'entité Image
        // $em->pesrist($image);

        // Etape 1 ter : pour cette relation pas de cascade lorsqu'on persiste Advert, car la relation est définie dans l'entité Application et non Advert. On doit donc tout persister à la main
        $em->persist($application1);
        $em->persist($application2);

        // Etape 2 : On déclenche l'enregistrement en BDD
        $em->flush();

        
        // SI la requete est en POST, c'est que le visiteur à soumis le formulaire
        if ($request->isMethod('POST')) {

            // Ici, on s'occupera de la création et de la gestion du formulaire

            // Création d'un message flash
            $session = $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistré');

            // Puis on redirige vers la page de visualisaton de cette annonce
            return $this->redirectToRoute('oc_platform_view',
                array('id' => $advert->getId())
            );

        }

        // SI on est pas en POST, ON AFFICHE LE FORMULAIRE
        return $this->render('OCPlatformBundle:Advert:add.html.twig',
            array(
                'advert' => $advert
            )
        );
    }

    /**
     * Edit Action
     *
     * @param $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        // On récupère l'annoce $id
        $advert = $em->getRepository('OCPlatformBundle:Advert')->find($id);

        if($advert === null)  {
            throw new NotFoundHttpException("L'annonce d'id " . $id . " n'existe pas");
        }

        // La méthode findAll retorune toutes les catégores de la BDD
        $listCategories = $em->getRepository('OCPlatformBundle:Category')->findAll();

        // On boucle sur les catégories pour les lier à l'annonce
        foreach($listCategories as $category) {
            $advert->addCategory($category);
        }

        // Pour persister le changement dans la relation, il faut persister l'entité propriétaire
        // Ici , Advert est la propriétaire, donc inutile de la persister car on l'a récupérer avec Doctrine

        // Etape 2 : On declenche l'enregistrement en BDD
        $em->flush() ;


        // Vérifie si la requête est en POST
        if ($request->isMethod('POST'))
        {
            $session = $request->getSession()->getFlashBag()->add('notice', 'Annonce bien enregistré');

            return $this->redirectToRoute('oc_platform_view',
                array('id' => 5)
            );
        }

        // On est pas en POST
        return $this->render('OCPlatformBundle:Advert:edit.html.twig',
            array(
                'advert' => $advert
            )
        );
    }


    /**
     * Delete Action
     *
     * @param $id
     * @return Response
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        // On reupere l'annonce $id
        $advert = $em->getRepository('OCPlatformBundle:Advert')->find($id);

        if($advert === null)  {
            throw new NotFoundHttpException("L'annonce d'id " . $id . " n'existe pas");
        }

        // On boucle sur les catégories de l'annonce pour les supprimmer
        foreach($advert->getCategories() as $category) {
            $advert->removeCatgory($category);
        }

        // Pour persister le changement dans la relation, il faut persister l'entité propriétaire
        // Ici , Advert est la propriétaire, donc inutile de la persister car on l'a récupérer avec Doctrine

        // On declenche la modification en BDD
        $em->flush();

        return $this->render('OCPlatformBundle:Advert:delete.html.twig');
    }

    /**
     * Menu Action
     *
     * @return Response
     */
    public function menuAction($limit)
    {
        // On fxe en dur une liste ici, bien entendu par la suite on la récupérera depuis la BDD
        $listAdverts = array(
            array('id' => 2, 'title' => 'Recherche développeur Symfony'),
            array('id' => 5, 'title' => 'Mission de Webmaster'),
            array('id' => 9, 'title' => 'Offre de stage de WebDesigner')
        );

        return $this->render('OCPlatformBundle:Advert:menu.html.twig',
            array(
                // Tou l'intérêt est ici : le controleur passe les variables nécessaires au template
                'listAdverts' => $listAdverts
            ));
    }

    /**
     * Modify a image from a Advert
     *
     * @param $advertId
     */
    public function editImageAction($advertId)
    {
        $em = $this->getDoctrine()->getManager();

        // On récupère l'annonce
        $advert = $em->getRepository('OCPlatformBundle:Advert')->find($advertId);

        // On modifie l'URL de l'image par exemple
        $advert->getImage()->setUrl('test.png');

        // On n'a ni besoin de persister l'annonce ni l'image
        // Rappellez vous ces entités sont automatiquement persistées car on les a récupérées depuis Doctrine lui même

        // On dévlenche la modification en BDD
        $em->flush();

        return new Response('OK');
    }
}